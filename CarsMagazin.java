import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CarsMagazin {
    public static void main(String[] args) {
        CarsMagazin carsMagazin = new CarsMagazin();
        carsMagazin.run();
    }

    public void run() {
        ArrayList<Cars> carsList = new ArrayList<>();
        Cars mercedes = new Cars("Mercedes CLS", 350, "coupe", true);
        Cars bmw = new Cars("BMW M5", 350, "sedan", true);
        Cars audi = new Cars("AUDI Q7", 250, "jeep", false);
        carsList.add(mercedes);
        carsList.add(bmw);
        carsList.add(audi);
        Comparator speedCarComparator = new CarsSpeedComparator();
        Collections.sort(carsList, speedCarComparator);
        System.out.println(carsList);
    }

}
