public class Cars implements Comparable<Cars> {
    private String name;
    private int speed;
    private String carcase;
    private boolean isRacing;

    public Cars(String name, int speed, String carcase, boolean isRacing) {
        this.name = name;
        this.speed = speed;
        this.carcase = carcase;
        this.isRacing = isRacing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getCarcase() {
        return carcase;
    }

    public void setCarcase(String carcase) {
        this.carcase = carcase;
    }

    public boolean isRacing() {
        return isRacing;
    }

    public void setRacing(boolean racing) {
        isRacing = racing;
    }

    @Override
    public String toString() {
        return name + '-' + isRacing + "\n" +
                "Speed " + speed + "\n" +
                "Carcase " + carcase;
    }

    @Override
    public int compareTo(Cars o) {
        return this.name.compareTo(o.getName());
    }
    
}
