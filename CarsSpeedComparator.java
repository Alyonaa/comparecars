import java.util.Comparator;

public class CarsSpeedComparator implements Comparator<Cars> {
    CarsCarcaseComparator carsCarcaseComparator = new CarsCarcaseComparator();

    @Override
    public int compare(Cars o1, Cars o2) {
        if (o2.getSpeed() == o1.getSpeed()) {
            return carsCarcaseComparator.compare(o1, o2);
        }
        return o2.getSpeed() - o1.getSpeed();
    }
}