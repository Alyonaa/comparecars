import java.util.Comparator;

public class CarsCarcaseComparator implements Comparator<Cars> {
    CarsIsRacingComparator carsIsRacingComparator = new CarsIsRacingComparator();

    @Override
    public int compare(Cars o1, Cars o2) {
        if (o2.getCarcase().equals(o1.getCarcase())) {
            return carsIsRacingComparator.compare(o1, o2);
        }
        return o1.getCarcase().compareTo(o2.getCarcase());
    }
}