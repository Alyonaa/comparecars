import java.util.Comparator;

public class CarsIsRacingComparator implements Comparator<Cars> {

    @Override
    public int compare(Cars o1, Cars o2) {
        if (o1.isRacing() == o2.isRacing()) {
            return 0;
        }
        if (o2.isRacing()) {
            return 1;
        }
        return -1;
    }
}
